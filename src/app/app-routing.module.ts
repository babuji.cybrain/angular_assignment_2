import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path:'authentication', loadChildren:() => import('./auth-module/auth-module.module')
    .then(mod=> mod.AuthModuleModule)
  },
  {
    path:'dashboard', loadChildren:() => import('./home-module/home-module.module')
    .then(mod=> mod.HomeModuleModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
