import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthModuleRoutingModule } from './auth-module-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {SignUpComponent} from './SignUp/SignUp.component';


@NgModule({
  declarations: [
    SignUpComponent
  ],
  imports: [
    CommonModule,
    AuthModuleRoutingModule,
    FormsModule,
    ReactiveFormsModule 
  ]
})
export class AuthModuleModule { }
