import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-SignUp',
  templateUrl: './SignUp.component.html',
  styleUrls: ['./SignUp.component.css']
})
export class SignUpComponent implements OnInit {

  valid:string = '';

  loginform:FormGroup;

  constructor(private fb:FormBuilder) {

    this.loginform = fb.group({
      f_name:['',[Validators.required]],
      l_name:['',[Validators.required]],
      email:['',[Validators.required, Validators.email]],
      addres:['',[Validators.required]]
    })

  }

  

  // f_nameValid = this.loginform.get('f_name');
  

  // l_nameValid = this.loginform.get('l_name');

  // emailValid = this.loginform.get('email');


  // addValid = this.loginform.get('addres');
  

  submitClicked(){
    if(this.loginform.valid){
      this.valid = 'submited';
    }
    else{
      for(const i in this.loginform.controls){
        this.loginform.controls[i].markAsTouched();
      } 
    }
  }


  

  ngOnInit() {
  }

}
