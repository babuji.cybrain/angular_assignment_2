import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {SignUpComponent} from './SignUp/SignUp.component';

const routes: Routes = [
  { path: 'authentication/sign-up/submited', redirectTo: 'dashboard/analytics', pathMatch: 'full' },
  {path: '',  redirectTo: 'authentication/sign-up', pathMatch: 'full'},
  {path:'sign-up', component:SignUpComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthModuleRoutingModule { }
